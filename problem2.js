const fs = require('fs')

function problem2(inputDataPath,outputFolderPath) {

    fs.mkdir(outputFolderPath, (error) => {

        if (error) {
            console.log(error)
        }
        else {

            fs.readFile(inputDataPath, 'utf8', (err, data) => {
                if (err) {
                    console.error(err)
                    return
                } else {

                    fs.writeFile(`${outputFolderPath}/lipsumUpperCase.txt`, data.toUpperCase(), 'utf-8', (error) => {

                        if (error) {
                            console.log(error)
                        }
                        else {

                            // console.log('File written successfully')

                            fs.writeFile(`${outputFolderPath}/filenames.txt`, 'lipsumUpperCase.txt', 'utf-8', (error) => {

                                if (error) {
                                    console.log(error)
                                } else {
                                    fs.readFile(`${outputFolderPath}/lipsumUpperCase.txt`, 'utf8', (err, data) => {

                                        if (error) {
                                            console.log(error)
                                        }
                                        else {

                                            let lines = data.toLowerCase().split(`.\n\n`).join('. ').split(`.${' '}`)

                                            let lipsumLowerCaseSentences = lines.reduce((accumulator, element) => {

                                                if (element == '') {
                                                    return accumulator
                                                } else {
                                                    return accumulator += `${element}.\n`
                                                }

                                            }, '')

                                            fs.writeFile(`${outputFolderPath}/lipsumLowerCaseSentences.txt`, lipsumLowerCaseSentences, 'utf-8', (error) => {

                                                if (error) {
                                                    console.log(error)
                                                }
                                                else {

                                                    // console.log('File written successfully')

                                                    fs.appendFile(`${outputFolderPath}/filenames.txt`, '\nlipsumLowerCaseSentences.txt', (error) => {
                                                        if (error) {
                                                            console.log(error)
                                                        }
                                                        else {

                                                            fs.readFile(`${outputFolderPath}/lipsumLowerCaseSentences.txt`, 'utf-8', (error, data) => {

                                                                if (error) {
                                                                    console.log(error)
                                                                }
                                                                else {

                                                                    sortedData = data.split('\n').sort().join(`\n`)
                                                                    fs.writeFile(`${outputFolderPath}/lipsumSortedSentences.txt`, sortedData, 'utf-8', (error) => {
                                                                        if (error) {
                                                                            console.log(error)
                                                                        }
                                                                        else {

                                                                            fs.appendFile(`${outputFolderPath}/filenames.txt`, '\nlipsumSortedSentences.txt', (error) => {

                                                                                if (error) {
                                                                                    console.log(error)
                                                                                } else {

                                                                                    fs.readFile(`${outputFolderPath}/filenames.txt`, 'utf-8', (error, data) => {

                                                                                        if (error) {
                                                                                            console.log(error)
                                                                                        }
                                                                                        else {

                                                                                            const filenames = data.split('\n')

                                                                                            // filenames.forEach((file) => {
                                                                                            //     fs.rm(`${outputFolderPath}/${file}`, (error) => {
                                                                                            //         if (error) {
                                                                                            //             console.log(error)
                                                                                            //         }
                                                                                            //     })
                                                                                            // })
                                                                                        }
                                                                                    })
                                                                                }
                                                                            })
                                                                        }
                                                                    })
                                                                }
                                                            })
                                                        }
                                                    })
                                                }
                                            })
                                        }
                                    })
                                }
                            })
                        }
                    })
                }
            })
        }
    })
}

module.exports = problem2
const problem2 = require('../problem2')
const path = require('path')

const outputPath = path.join(__dirname,'../output/output2')
const inputDataPath = path.join(__dirname,'../dataFile/lipsum.txt')


problem2(inputDataPath,outputPath)
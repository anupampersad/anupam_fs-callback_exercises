const fs = require('fs')

function problem1(outputFolderPath){
    
    fs.mkdir(outputFolderPath,(error)=>{
        if(error){
            console.log(error)
        }else{
            let data1 = {
                name:'Anupam',
                age:'25 years old',
                isCoder:true
            }
            fs.writeFile(`${outputFolderPath}/data1.json`,JSON.stringify(data1),'utf-8',(err)=>{
                if (err) { 
                    console.log(err)
                }else{
                    console.log('File 1 Created successfully')
    
                    fs.rm(`${outputFolderPath}/data1.json`,(error)=>{
                        if (error){
                            console.log(error)
                        }else{
                            console.log('File 1 Removed Successfully')
                        }
                    } )
    
                    let data2 = {
                        name:'Conor McGregor',
                        age:"32 years old",
                        isCoder:false
                    }
    
                    fs.writeFile(`${outputFolderPath}/data2.json`,JSON.stringify(data2),'utf-8',(err)=>{
                        if(error){
                            console.log(error)
                        }
                        else{
                            console.log('File 2 created successfully')
    
                            fs.rm(`${outputFolderPath}/data2.json`,(error)=>{
                                if (error){
                                    console.log(error)
                                }else{
                                    console.log('File 2 Removed Successfully')
                                }
                            } )
                        }
                    })
    
    
                }
            })
        }
    })

}

module.exports = problem1
